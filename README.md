# pdfrename

This script takes a PDF file (as an argument) and renames that file in place with the following information from its metadata: title and author, if the PDF has them.
The script depends on [pypdf](https://pypi.org/project/pypdf/) and [pathvalidate](https://pypi.org/project/pathvalidate/).

Options:

* `--date` the creation date of the PDF will be appended in [ISO](https://en.wikipedia.org/wiki/ISO_8601) format, if the PDF has one
* `--today` the current date will be appended in [ISO](https://en.wikipedia.org/wiki/ISO_8601) format

If you use both options `--today` is ignored.