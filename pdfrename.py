#!/usr/bin/env python3
"""
Rename a PDF with information taken from its metadata.
Depends on pypdf: https://pypi.org/project/pypdf/
Depends on pathvalidate: https://pypi.org/project/pathvalidate/
"""

import argparse
import datetime
import os
import pathvalidate
import pypdf


ISO8601 = "%Y-%m-%d"


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("target")
    parser.add_argument("--date", action="store_true")
    parser.add_argument("--today", action="store_true")
    args = parser.parse_args()
    with open(args.target, "rb") as file:
        metadata = pypdf.PdfReader(file).metadata
    new_name = []
    if metadata.title:
        new_name.append(metadata.title)
    if metadata.author:
        new_name.append(metadata.author)
    if metadata.creation_date and args.date:
        new_name.append(metadata.creation_date.strftime(ISO8601))
    if args.today and not args.date:
        new_name.append(datetime.datetime.today().strftime(ISO8601))
    if new_name:
        sanitized_name = pathvalidate.sanitize_filename("_".join(new_name)).replace(
            " ", "_"
        )
        os.rename(args.target, f"{sanitized_name}.pdf")